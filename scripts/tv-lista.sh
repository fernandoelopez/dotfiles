#!/bin/sh

channels=$(cat) <<CHANNELS
ARGENTINA
TN HD
rtsp://stream.tn.com.ar/live/tnhd1
AMERICA TV
http://iphone-streaming.ustream.tv/uhls/14031537/streams/live/iphone/playlist.m3u8
CANAL 26 ARGENTINA
mms://200.115.194.1/canal26?mswmext=.asf
tv publica
rtmp://sh002-0.streamcdn.eu/c2s_test/ playpath=52f512fe691d0.stream swfUrl=http://p.jwpcdn.com/6/8/jwplayer.flash.swf live=1
Deportv argentina
rtmp://sh001-1.streamcdn.eu/c2s_test/ playpath=52ddfb553c251.stream swfUrl=http://p.jwpcdn.com/6/8/jwplayer.flash.swf live=1
C5N
rtmp://c5n.stweb.tv:1935/c5n playpath=live swfUrl=http://c5n.minutouno.com/js/c5n/jwplayer/player.swf live=1 pageUrl=http://latelete.com/canales/argentina/cn5.php
El trece
rtsp://stream.eltrecetv.com.ar/live13/13tv/13tv1


PORTUGAL
RTP 1
rtmp://ec21.rtp.pt/livetv playpath=2ch5h264 swfUrl=http://www.rtp.pt/play/player.swf live=1 pageUrl=http://www.rtp.pt/noticias/votacao/?mysurvey=343r23
RTP 2
rtmp://ec21.rtp.pt/livetv playpath=2ch3h264 swfUrl=http://www.rtp.pt/play/player.swf live=1 pageUrl=http://www.rtp.pt/noticias/votacao/?mysurvey=343r23
RTP Internacional
rtmp://ec21.rtp.pt/livetv playpath=2ch120h264 swfUrl=http://www.rtp.pt/play/player.swf live=1 pageUrl=http://www.rtp.pt/noticias/votacao/?mysurvey=343r23
ESPAÑA
La 1
http://iphonelive.rtve.es/LA1_LV3_IPH/LA1_LV3_IPH.m3u8
La 2
http://iphonelive.rtve.es/LA2_LV3_IPH/bitrate_2.m3u8
Antena 3
opcion 1:
http://antena3-aos1-apple-live.adaptive.level3.net/apple/antena3/channel01/index.m3u8
opcion 2:
rtmp://antena3fms35livefs.fplive.net/antena3fms35live-live//stream-antena3_1
la sexta hd
opcion 1:
http://antena3-aos1-apple-live.adaptive.level3.net/apple/antena3/channel02/index.m3u8
opcion 2:
rtmp://antena3fms35livefs.fplive.net/antena3fms35live-live//stream-lasexta_1
Xplora
http://antena3-aos1-apple-live.adaptive.level3.net/apple/antena3/channel03/index.m3u8
Nova &Neox
http://antena3-aos1-apple-live.adaptive.level3.net/apple/antena3/channel12/index.m3u8
TELEDEPORTE
http://iphonelive.rtve.es/TDP_LV3_IPH/TDP_LV3_IPH.m3u8
TV3 CAT
http://www.tv3.cat/directetv3cat/tv3catpc.m3u8
SPORT 3 CAT
rtmp://tv-geoespanya-flashlivefs.fplive.net:1935/tv-geoespanya-flashlive-live?ovpfv=1.1/stream_ES_ES3_FLV playpath=stream_ES_ES3_FLV swfUrl=http://granhermano-14.com/reproduct...espanya-flashlive-live&file=stream_ES_ES3_FLV live=1 pageUrl=http://hispaniatelevision.com/TV/Deportes/sport3.htm
GALICIA TV
http://media3.crtvg.es/live/tvge/live.m3u8
EXTREMADURA TV
rtmp://corporacionextremenaocclivefs.fplive.net/corporacionextremenalive-live/ playpath=stream1 swfUrl=http://www.canalextremadura.es/site...slx_reproductor/js/mediaplayer-5.7/player.swf live=1 pageUrl=http://www.canalextremadura.es/alacarta/tv/directo
LEVANTE TV
rtmp://teledifusion.tv:1935/levantetvlive playpath=levantetvlive swfUrl=http://www.levantetv.es//servicios/...evantetvlive&autostart=true&wmode=transparent live=1 pageUrl=http://www.levantetv.es//servicios/...evantetvlive&autostart=true&wmode=transparent --live
TV CANARIAS
http://streamrtvcnet.mad.idec.net/rtvcnet/rtvcnet.stream/live.m3u8
CASTILLA Y LEON
rtmp://cdn.s10.eu.nice264.com/niceLiveServer/cyl_cyltvlivem_MB_698
CASTILLA LA MANCHA TV
rtmp://teledifusion.tv/rtvcm playpath=rtvcmlive1 swfUrl=http://rtvcm.es/templates/marco/endirecto/player.swf live=1 pageUrl=http://rtvcm.es/templates/marco/endirecto/
ANDALUCIATV
http://iphone-andaluciatelevision.rtva.stream.flumotion.com/rtva/andaluciatelevision-iphone-multi/main.m3u8
HUELVA TV
rtmp://flash3.todostreaming.es/huelvatv playpath=livestream swfUrl=http://www.huelvatv.com/plugins/con....es/huelvatv&autostart=true&wmode=transparent live=1 pageUrl=http://www.huelvatv.com/plugins/con....es/huelvatv&autostart=true&wmode=transparent –live
ARAGON TV
rtmp://aragontvlivefs.fplive.net/aragontvlive-live playpath=stream_normal_abt swfUrl=http://alacarta.aragontelevision.es/streaming/flowplayer.commercial-3.2.7.swf live=1 pageUrl=http://alacarta.aragontelevision.es/streaming/streaming.html --live

VENEZUELA
ENERGIA VISION TV
http://edge.wms28.lorini.net/EnergiaVision/EnergiaVision1/live.m3u8
GLOBOVISION
http://edge.wms28.lorini.net/globovisiontv/globovisiontv/live.m3u8
LATELE
http://edge.wms28.lorini.net/latele/latele/live.m3u8
PROMAR TV
http://edge.wms28.lorini.net/promartv/promartv/live.m3u8
TELECOLOR
http://edge.wms28.lorini.net/telecolor/telecolor/live.m3u8
TRT TV
http://edge.wms28.lorini.net/trttv/trttv/live.m3u8
TRV TV
http://edge.wms28.lorini.net/tvrtv/tvrtv/live.m3u8
TVR VENEZUELA
http://edge.wms28.lorini.net/tvrtv/tvrtv/live.m3u8
VALE TV
http://edge.wms28.lorini.net/valetv/valetv/live.m3u8

MEXICO
UNIVISION
rtmp://edge3.video.vaughnsoft.net:443/live? playpath=live_univision_hd13 swfUrl=http://vaughnlive.tv/800021294/swf/VaughnSoftPlayer.swf live=1 pageUrl=http://vaughnlive.tv/embed/video/univision_hd13?viewers=true&watermark=left&autoplay=true token=30dabc4871922a1314192e925ab7961d
MULTIMEDIOS TV
http://brightcove03-f.akamaihd.net/multimedios_tv_612k@55732?file=ChelTV_m3u8
Mexico Nuestravision
rtsp://video.stream.mx/8058/8058
MEXICO - AZTECA NOTICIAS
http://urtmpkal-f.akamaihd.net/i/077zoqxzy_1@130091/index_3_av-p.m3u8
MEXICO - MONTERREY
http://brightcove03-f.akamaihd.net/multimedios_tv_612k@55732?file=ChelTV_m3u8
Milenio tv
http://brightcove03-f.akamaihd.net/milenio_centro_512k@101956?file=cheltv_m3u8
AZTECATRECE HD
http://urtmpkal-f.akamaihd.net/i/022fq4x8p_1@130156/index_3_av-b.m3u8
TVPOLANCO
rtmp://190.0.28.154:1935/live/ playpath=mWbaHbOjua swfUrl=http://www.longtailvideo.com/jwplayer/jwplayer.flash.swf live=1
ONCETV
rtmp://www.oncetvmexicolive.tv:1935/livepkgr2 playpath=int3 swfUrl=http://fpdownload.adobe.com/strobe/FlashMediaPlayback_101.swf/[[DYNAMIC]]/1 live=1 pageUrl=http://www.stream2video.tv/getStream/137/1
MEXIQUENSE
rtmp://fss28.streamhoster.com:1935/lv_tvmexiquense/_definst_ playpath=broadcast1 swfUrl=http://fpdownload.adobe.com/strobe/FlashMediaPlayback_101.swf/[[DYNAMIC]]/1 live=1 pageUrl=http://www.stream2video.tv/getStream/134/1
CHILE
CHILE CONTIVISION
http://v1.tustreaming.cl:1935/contivision/contivision/live.m3u8
hd:
rtmp://stream1.eltelon.com:80/live/contivision/ playpath=contivision671 swfUrl=http://www.eltelon.com/resources/player/liveplayer/jwplayer.flash.swf live=1 pageUrl=http://www.eltelon.com/contivision/contivision/embed
CHILE TVCHILE
rtsp://wow.tvn.cl:1935/envivo_edge/tvchile_m
CHILE CANAL 34
http://upmv10.comcast.upmv.ustream.tv/0/1/40/40611/40611222/1_11176799_40611222.flv?tracking=ad6f16_61_1_0_0&src=ro&hash=7ae0
CHILE IQUIQUE
http://upmv10.comcast.upmv.ustream.tv/0/1/42/42453/42453285/1_15021669_42453285.flv?tracking=ad6f16_4_1_0_0&src=ro&hash=2919

ECUADOR
ECUADORTV
rtmp://38.96.148.216:1935/live playpath=ecuadortvn3 swfUrl=http://www.ecuadortv.ec/mod_video/player.swf live=1
TELEAMAZONAS
rtmp://173.193.197.46/flash playpath=16295 swfUrl=http://www.udemy.com/static/flash/player5.9.swf live=1 pageUrl=http://sawlive.tv/embed/watch/NTU4ZDY5NDQ1NjQzOmE5NjU2YzY1NjE2ZDYxN2E2ZjZlNjE3Mz JkNjU2ZTJkNzY2OT/c2NmY6ZjlmMTBlNTJjMDY1ZDNlNTc0ZmRiMTk1NTljYWY1Zjdk OGU4NmQxYzphMDBkOTk5MDM1ZmU1NmJjYWNiODRkZTVlMjIxNT RiZg__

PANAMA
CABLE HONDA SPORTS
rtmp://173.239.7.228/blcdcos/live/ playpath=cosHD_360p swfUrl=http://p.jwpcdn.com/6/8/jwplayer.flash.swf live=1

COLOMBIA
win sports colombia
rtmp://cdn8.iviplanet.com/WinsportsHD/ playpath=myStream.sdp swfUrl=http://p.jwpcdn.com/6/8/jwplayer.flash.swf live=1
OTROS
FOX SPORT
http://m24.megafun.vn/live.vs?c=vstv042&q=high
baloncesto l.A
http://xrxs.net/video/live-p12losa-2328.m3u8
OUTDOOR SPORTS
http://www.brdog.net/brdog_stv/sports/outdoor_sport.asx
TRACE SPORTS
http://lswb-de-08.servers.octoshape.net:1935/live/kanalsport_500k/live.m3u8
Animal Planet USA
http://iphone-streaming.ustream.tv/uhls/12762028/streams/live/iphone/live.m3u8
CHANNELS

tmp=$(mktemp)
urls=$(mktemp)
opt=$(mktemp)
i=1
echo "$channels" | grep -E '.+://.+' -B 1 | grep -v -E  '.+://.+' -A 1 \
| while read line; do
    [ -n "$(echo $line | grep -E '.+://.+')" ] && continue
    read url
    [ -z "$(echo $url | grep -E '.+://.+')" ] && continue
    echo "$i $(echo $line | tr ' ' '_')"
    echo "$url" >> $urls
    i=$(expr $i + 1)
done >> $tmp

dialog --menu Canales 0 0 0 $(cat $tmp) 2> $opt

if [ -z "$1" ] || [ "$1" = mplayer ]; then
    mplayer -autosync 30 -cache 512 "$(sed "$(cat $opt)q;d" $urls)"
else
    vlc "$(sed "$(cat $opt)q;d" $urls)"
fi

rm $tmp $urls $opt

