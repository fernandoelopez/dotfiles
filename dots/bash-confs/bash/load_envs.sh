alias printf='true'
printf 'Cargando envs:'
for lang in rb py pl php nd d j lua go any; do
    tool="${lang}env"
    if [ ! -e ~/.$tool ]; then
        printf " \033[2;31m$tool\033[0m"
        continue
    fi
    if [ -z "$(echo $PATH | grep $tool)" ]; then
        export PATH="~/.$tool/bin:$PATH"
    fi
    printf " \033[1;32m$tool\033[0m"
    eval "$(~/.$tool/bin/$tool init -)"
    case $lang in
        rb)
            alias be='bundle exec'
            alias tbe='env RACK_ENV=test bundle exec'
            alias dbe='env RACK_ENV=development bundle exec'
            alias pbe='env RACK_ENV=production bundle exec'
            ;;
    esac
done
printf '\n'
unalias printf
