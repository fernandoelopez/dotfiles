load_envs

alias grep='grep --color'
set -x DEBFULLNAME 'CHANGE ME'
set -x DEBEMAIL 'CHANGE@ME'
set -x PATH "$HOME/scripts" "$HOME/.local/bin" $PATH
if [ -d ~/.cabal/bin ]
    set -x PATH "$HOME/.cabal/bin" $PATH
end

set -x BROWSER links
set -x PYTHONSTARTUP "$HOME/.pystartup"
set -x EDITOR vim
