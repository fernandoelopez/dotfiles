if not set -q __GIT_PROMPT_DIR
    set __GIT_PROMPT_DIR ~/.gitprompt
end

# Colors
# Reset
set ResetColor (set_color normal)       # Text Reset

# Regular Colors
set Red (set_color red)                 # Red
set Yellow (set_color yellow);          # Yellow
set Blue (set_color blue)               # Blue
set WHITE (set_color white)

# Bold
set BGreen (set_color -o green)         # Green
set BYellow (set_color -o yellow);      # Yellow
set BBlue (set_color -o blue);          # Blue
set BRed (set_color -o red);            # Red

# High Intensty
set IBlack (set_color -o black)         # Black

# Bold High Intensty
set Magenta (set_color -o purple)       # Purple

# Default values for the appearance of the prompt. Configure at will.
set GIT_PROMPT_PREFIX "["
set GIT_PROMPT_SUFFIX "]"
set GIT_PROMPT_SEPARATOR "|"
set GIT_PROMPT_BRANCH "$Magenta"
set GIT_PROMPT_STAGED "$Red● "
set GIT_PROMPT_CONFLICTS "$Red✖ "
set GIT_PROMPT_CHANGED "$Blue✚ "
set GIT_PROMPT_REMOTE " "
set GIT_PROMPT_UNTRACKED "…"
set GIT_PROMPT_STASHED "⚑ "
set GIT_PROMPT_CLEAN "$BGreen✔"

function fish_prompt
    if test "$status" -ne 0
        set PROMPT_END "$BRed\$ "
    else
        set PROMPT_END "\$ $ResetColor"
    end

    if not set -q __fish_prompt_hostname
        set -g __fish_prompt_hostname (hostname -s)
    end

    set PROMPT_START "$BYellow><> $BGreen$USER"@"$__fish_prompt_hostname$WHITE:$ResetColor$BBlue"(prompt_pwd)"$ResetColor"

    set -e __CURRENT_GIT_STATUS
    set gitstatus "$__GIT_PROMPT_DIR/gitstatus.py"

    set -e __GIT_PROMPT_FALLBACK
    for each in $SLOW_GITSTATUS_DIRS
        if [ "$each" = "$PWD" ]
            set -g __GIT_PROMPT_FALLBACK (__fish_git_prompt)
            if [ -n "$__GIT_PROMPT_FALLBACK" ]
                set PROMPT_END "$__GIT_PROMPT_FALLBACK$PROMPT_END"
            end
            break
        end
    end

    if not set -q __GIT_PROMPT_FALLBACK
        set before (date +%s)
        set _GIT_STATUS (python $gitstatus)
        set __CURRENT_GIT_STATUS $_GIT_STATUS
        if test (expr (date +%s) - "$before") -ge 3
            set -U SLOW_GITSTATUS_DIRS $SLOW_GITSTATUS_DIRS "$PWD"
        end
    end

    set __CURRENT_GIT_STATUS_PARAM_COUNT (count $__CURRENT_GIT_STATUS)

    if not test "0" -eq $__CURRENT_GIT_STATUS_PARAM_COUNT
        set GIT_BRANCH $__CURRENT_GIT_STATUS[1]
        set GIT_REMOTE "$__CURRENT_GIT_STATUS[2]"
        if contains "." "$GIT_REMOTE"
            set -e GIT_REMOTE
        end
        set GIT_STAGED $__CURRENT_GIT_STATUS[3]
        set GIT_CONFLICTS $__CURRENT_GIT_STATUS[4]
        set GIT_CHANGED $__CURRENT_GIT_STATUS[5]
        set GIT_UNTRACKED $__CURRENT_GIT_STATUS[6]
        set GIT_STASHED $__CURRENT_GIT_STATUS[7]
        set GIT_CLEAN $__CURRENT_GIT_STATUS[8]
    end

    if test -n "$__CURRENT_GIT_STATUS"
        set STATUS " $GIT_PROMPT_PREFIX$GIT_PROMPT_BRANCH$GIT_BRANCH$ResetColor"

        if set -q GIT_REMOTE
            set STATUS "$STATUS$GIT_PROMPT_REMOTE$GIT_REMOTE$ResetColor"
        end

        set STATUS "$STATUS$GIT_PROMPT_SEPARATOR"

        if [ $GIT_STAGED != "0" ]
            set STATUS "$STATUS$GIT_PROMPT_STAGED$GIT_STAGED$ResetColor"
        end

        if [ $GIT_CONFLICTS != "0" ]
            set STATUS "$STATUS$GIT_PROMPT_CONFLICTS$GIT_CONFLICTS$ResetColor"
        end

        if [ $GIT_CHANGED != "0" ]
            set STATUS "$STATUS$GIT_PROMPT_CHANGED$GIT_CHANGED$ResetColor"
        end

        if [ "$GIT_UNTRACKED" != "0" ]
            set STATUS "$STATUS$GIT_PROMPT_UNTRACKED$GIT_UNTRACKED$ResetColor"
        end

        if [ "$GIT_STASHED" != "0" ]
            set STATUS "$STATUS$GIT_PROMPT_STASHED$GIT_STASHED$ResetColor"
        end

        if [ "$GIT_CLEAN" = "1" ]
            set STATUS "$STATUS$GIT_PROMPT_CLEAN"
        end

        set STATUS "$STATUS$ResetColor$GIT_PROMPT_SUFFIX"

        set PS1 "$PROMPT_START$STATUS$PROMPT_END"
    else
        set PS1 "$PROMPT_START$PROMPT_END"
    end

    echo -e $PS1

end


