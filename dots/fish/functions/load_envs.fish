function load_envs
    function printf
        # command printf $argv
    end
    printf 'Cargando envs:'
    for lang in rb pl py php nd d j lua go any
        set -l tool {$lang}env
        if [ ! -e ~/.$tool ];
            printf " \033[2;31m$tool\033[0m"
            continue
        end
        if [ -z (echo $PATH | grep $tool) ]
            set -x PATH ~/.$tool/shims ~/.$tool/bin $PATH
        end
        if [ "$lang" != nd ]
            printf " \033[1;32m$tool\033[0m"
            source (eval $tool init -|psub)
        else
            printf " \033[1;33m$tool\033[0m"
        end
        switch $lang
            case rb
                alias be='bundle exec'
                alias tbe='env RACK_ENV=test bundle exec'
                alias dbe='env RACK_ENV=development bundle exec'
                alias pbe='env RACK_ENV=production bundle exec'
        end
    end
    printf '\n'
    functions -e printf
end
